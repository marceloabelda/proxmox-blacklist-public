
#!/bin/bash

#correr con --test para prueba
#bloquear cidr
#sudo pmgsh create /config/ruledb/who/2/network --cidr 1.1.1.1/32 



es_direccion_ip_o_red() {
  local entrada="$1"
  
  # Expresión regular para una dirección IP en formato xxx.xxx.xxx.xxx
  ip_regex="^([0-9]{1,3}\.){3}[0-9]{1,3}$"

  # Expresión regular para una dirección de red en formato xxx.xxx.xxx.xxx/yy
  red_regex="^([0-9]{1,3}\.){3}[0-9]{1,3}/[0-9]{1,2}$"

  if [[ $entrada =~ $ip_regex ]]; then
    echo "Es una dirección IP válida."
    ejecutar_o_mostrar "$1" "pmgsh create '/config/ruledb/who/2/ip' --ip $linea"
  elif [[ $entrada =~ $red_regex ]]; then
    echo "Es una dirección de red válida."
    ejecutar_o_mostrar "$1" "pmgsh create '/config/ruledb/who/2/network' --cidr $linea"  
  else
    echo "No es una dirección IP ni una dirección de red válida."
  fi
}



ejecutar_comando_en_lineas() {
  local archivo="$1"

  # Verificar si el archivo existe
  if [ ! -f "$archivo" ]; then
    echo "El archivo $archivo no existe."
    return 1
  fi

  while IFS= read -r linea; do
    # Ejecutar el comando para cada línea
    es_direccion_ip_o_red $linea
  done < "$archivo"
}

# Uso: ejecutar_comando_en_lineas archivo.txt


# URL del archivo remoto
remote_file_url="https://gitlab.com/marceloabelda/proxmox-blacklist-public/-/raw/main/network-blacklist.txt"

mkdir -p /srv/blacklists

# Ruta al archivo local
local_file_path="/srv/blacklists/network-blacklist.txt"

# Descargar el archivo remoto
wget -q "$remote_file_url" -O network-blacklist.txt.temp

ejecutar_o_mostrar() {
    local test="$1"
    local comando="$2"
    if [ "$test" = "--test" ]; then
      
      echo En modo test: "$comando"
    else 
      resultado=`$comando`
      echo "$resultado"
    fi

}


# Comprobar si la descarga fue exitosa
if [ $? -eq 0 ]; then
  # Comparar el archivo descargado con el archivo local
  if ! cmp -s "network-blacklist.txt.temp" "$local_file_path"; then
    # Los archivos son diferentes, reemplazar el archivo local
    mv "network-blacklist.txt.temp" "$local_file_path"
    echo "El archivo se ha actualizado."
    ejecutar_comando_en_lineas $local_file_path
  else
    # Los archivos son iguales, eliminar el temporal
    rm "network-blacklist.txt.temp"
    echo "El archivo no ha cambiado."
    # Comprueba si se proporciona el argumento --test
    ejecutar_o_mostrar "$1" "ejecutar_comando_en_lineas $local_file_path" 
    ejecutar_comando_en_lineas $local_file_path
    
  fi
else
  echo "Error al descargar el archivo remoto."
fi


